The testng4esper Library.
==============

(C) 2013 University of Zurich, Department of Informatics

This software comes with no warranty.

Licensing
--------------

This software is licensed under the GNU General Public License, version 2 
(GPLv2)or any later version. Note that under the current licensing terms of 
Esper, which is licensed under GPLv2, any derivative work must be distributed 
under GPLv2.


Acknowledgements
--------------

The research leading to these results has received funding 
from the European Union Seventh Framework Programme FP7/2007-2011 
under grant agreement n° 296126.

Summary
--------------
This package defines testng tests for the Esper complex event processing 
engine. These tests read an Esper configuration and one or more state-
ments. The actual business logic has to implemented by the specific test
classes that subclass from the test classes in this package.

Dependencies
--------------
This library uses the following other libraries:

- testng
- Esper 
- slf4j


Usage
--------------

	<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >

	<suite name="NAME OF YOUR TESTSUITE" verbose="1">

	<test name="NAME_OF_YOUR_TEST">
		<parameter name="esper-config" value="OPTIONAL_PATH_TO_ESPER_CONFIG" />
		<parameter name="statement-files-list" value="PATH_TO_STATEMENT_FILE" />
		<classes>
			<class name="NAME_OF_YOUR_CLASS_THAT_SUBCLASSES_ONE_TEST_CASE" />
		</classes>
	</test>

	</suite>
package ch.uzh.ifi.ddis.ifp.esper.test;

/*
 * #%L
 * TestNG4Esper
 * %%
 * Copyright (C) 2013 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

import org.testng.annotations.Test;

import com.espertech.esper.client.EPServiceProvider;

/**
 * Test for checking that the {@link EsperTest} works correctly.
 * <p>
 * This test ensures the following:
 * <ul>
 * <li>Adding statements from XML files to the {@link EPServiceProvider}</li>
 * </ul>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.3.1
 * @since 0.3.1
 * 
 */
public class TestEsperTest extends EsperTest {

	@Override
	protected void beforeConfigure() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void afterConfigure() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void beforeInitEngine() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void afterInitEngine() {
		// TODO Auto-generated method stub

	}

	@Test
	public void test() {
		_log.info("Finished testing testng4esper Test");
	}

}

package ch.uzh.ifi.ddis.ifp.esper.test;

/*
 * #%L
 * TestNG4Esper
 * %%
 * Copyright (C) 2013 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RandomJoinFlow implements Runnable {

	private static final Logger _log = LoggerFactory
			.getLogger(RandomJoinFlow.class);

	//
	//
	//

	private boolean _interrupted;
	private boolean _running;

	public RandomJoinFlow() {
		setInterrupted(true);
	}

	//
	//
	//

	public void run() {

		setRunning(true);
		setInterrupted(false);
		try {
			while (!isInterrupted()) {

			}
			if (isInterrupted()) {
				_log.info("Interrupted!");
			}
		} catch (Exception e) {

		} finally {
			setRunning(false);
		}
	};

	//
	//
	//

	public void setRunning(boolean running) {
		_running = running;
	}

	public boolean isRunning() {
		return _running;
	}

	public void setInterrupted(boolean interrupted) {
		_interrupted = interrupted;
	}

	public boolean isInterrupted() {
		return _interrupted;
	}

}

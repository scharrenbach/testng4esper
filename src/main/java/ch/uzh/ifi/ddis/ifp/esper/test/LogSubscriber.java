package ch.uzh.ifi.ddis.ifp.esper.test;

/*
 * #%L
 * TestNG4Esper
 * %%
 * Copyright (C) 2013 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.client.EPStatement;

/**
 * <p>
 * The {@link LogSubscriber} is a simple subscriber to the Esper complex event
 * processing engine and logs all updates for a statement.
 * </p>
 * <p>
 * See also the <a href=
 * "http://esper.codehaus.org/esper-4.9.0/doc/reference/en-US/html_single/#api-admin-subscriber"
 * >Esper reference docs, Sec. 14.3.3 "Setting a Subscriber Object"</a>
 * </p>
 * 
 * <h2>Usage</h2>
 * 
 * <pre>
 * EPAdministrator epAdmin = ...
 * for (String stmtName : epAdmin.getStatementNames()) { 
 *     final EPStatement stmt =
 *         epAdmin.getStatement(stmtName);
 *     stmt.setSubscriber(new LogSubscriber(stmt));
 * }
 * 
 * </pre>
 * 
 * @author Thomas Scharrenbach
 * @version 0.1.1
 * @since 0.1.1
 * 
 */
public class LogSubscriber {

	private static final Logger _log = LoggerFactory
			.getLogger(LogSubscriber.class);

	public final EPStatement statementName;

	public LogSubscriber(EPStatement stmt) {
		this.statementName = stmt;
	}

	public void update(Object[] values) {
		StringBuffer valuesBuffer = new StringBuffer();
		valuesBuffer.append("[");
		boolean firstVal = true;
		for (Object val : values) {
			if (firstVal) {
				firstVal = false;
			} else {
				valuesBuffer.append(", ");
			}
			valuesBuffer.append(String.format("%s", val.toString()));
		}
		valuesBuffer.append("]");
		_log.debug("Statement {}: Values: {}", this.statementName.getName(),
				valuesBuffer.toString());
	}

}
